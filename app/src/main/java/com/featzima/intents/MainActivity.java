package com.featzima.intents;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private final static int REQUEST_GENDER = 101;
    private final static int REQUEST_IMAGE = 104;

    private Button chooseGenderButton;
    private Button chooseImageButton;
    private Button setAlarmButton;
    private TextView genderTextView;
    private ImageView catImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        genderTextView = findViewById(R.id.genderTextView);
        catImageView = findViewById(R.id.catImageView);
        chooseGenderButton = findViewById(R.id.chooseGenderButton);
        chooseImageButton = findViewById(R.id.chooseImageButton);
        setAlarmButton = findViewById(R.id.setAlarmButton);

        chooseImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });
        chooseGenderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseGender();
            }
        });
        setAlarmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAlarm();
            }
        });
    }

    private void setAlarm() {
        AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlarmBroadcastReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(MainActivity.this, 0, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.SECOND, calendar.get(Calendar.SECOND) + 5);

        alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
    }

    private void chooseImage() {
        Intent implicitIntent = new Intent();
        implicitIntent.setType("image/*");
        implicitIntent.setAction(Intent.ACTION_GET_CONTENT);
        Intent chooserIntent = Intent.createChooser(implicitIntent, "Choose Image");
        startActivityForResult(chooserIntent, REQUEST_IMAGE);
    }

    private void chooseGender() {
        Intent explicitIntent = new Intent(this, SecondActivity.class);
        startActivityForResult(explicitIntent, REQUEST_GENDER);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_GENDER) {
                int genderIndex = data.getExtras().getInt(SecondActivity.EXTRA_GENDER, -1);
                Gender resultGender = Gender.values()[genderIndex];
                setGender(resultGender);
            }
            if (requestCode == REQUEST_IMAGE) {
                try {
                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    catImageView.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                }
            }
        }
    }

    private void setGender(Gender resultGender) {
        if (resultGender == Gender.Man) {
            genderTextView.setText(getString(R.string.man));
        }
        if (resultGender == Gender.Woman) {
            genderTextView.setText(getString(R.string.woman));
        }
    }
}
